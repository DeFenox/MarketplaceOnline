<%--
  Created by IntelliJ IDEA.
  User: DeFenox
  Date: 10.12.2016
  Time: 20:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <jsp:include page="IncludeFiles.jsp"/>
</head>
<body>
<div class="container">
    <form action="/user/" class="form-singin" id="loginForm" method="post">
        <input type="hidden" name="command" value="Login">
        <div class="control-group">
            <label class="control-label" for="inputLogin">Login</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="inputLogin" name="login" placeholder="Login"> <br/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPassword">Password </label>
            <div class="controls">
                <input type="password" class="input-xlarge" id="inputPassword" name="password">
            </div>
        </div>
    </form>
    <br/>
    <button class="btn btn-large btn-block btn-primary" type="submit" form="loginForm" value="singin" name="singin">Sing in</button>
    <button class="btn btn-large btn-block" type="submit" form="loginForm" name="guest" value="guest">Enter as a guest</button>
    <button class="btn btn-large btn-block" type="submit" form="loginForm" name="register" value="guest">Register</button>
</div>


<jsp:include page="ScriptsIncludes.jsp"/>
</body>
</html>

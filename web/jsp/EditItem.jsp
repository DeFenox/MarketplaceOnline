<%--
  Created by IntelliJ IDEA.
  User: DeFenox
  Date: 10.12.2016
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EditItem</title>
    <jsp:include page="IncludeFiles.jsp"/>
</head>
<body>
<div>
    <div class="container">

        <form action="/item/" class="form-edit" id="editForm" method="post">
            <input type="hidden" name="command" value="EditItem">

            <div class="control-group">
                <label class="control-label" for="inputTitleItem">Title of item:</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="inputTitleItem" name="inputTitleItem"> <br/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputDescription">Description:</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="inputDescription" name="description"> <br/>
                </div>
            </div>
            <br/>
            <div class="control-group">
                <label class="control-label" for="inputStartPrice">Start price:</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="inputStartPrice" name="startPrice"> <br/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputTimeLeft">Time Left:</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="inputTimeLeft" name="timeLeft"> <br/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputBuyItNow">By it now:</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="inputBuyItNow" name="buyItNow"> <br/>
                </div>
            </div>

        </form>
        <button class="btn btn-large btn-block btn-primary" type="submit" form="editForm" value="edit" name="edit">Edit</button>
    </div>
</div>

<jsp:include page="ScriptsIncludes.jsp"/>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ShowMyItems</title>
    <jsp:include page="IncludeFiles.jsp"/>
</head>
<body>
<div class="container">
    <div>
        <% if (session.getAttribute("uid") != null) { %>
        <div> You are logged in as
            <% Integer id = (Integer) session.getAttribute("uid");
                if (id == -1) { %>
            Guest
        </div>
        <div><a href="<%= request.getRemoteAddr() + "/user/?command=Login" %>">Login</a></div>

        <% } else { %>
        <b><%= session.getAttribute("uname") %>
        </b>
    </div>
    <div><a href="<%= request.getRemoteAddr() + "/user/?command=Logout" %>">Logout</a></div>

    <% } %>
    <% } else { %>
    <div><a href="<%= request.getRemoteAddr() + "/user/?command=Login" %>">Login</a></div>
    <% } %>
</div>

<div>
    <h1>Online marketplace</h1>
</div>

<div>
    <h3>Search parameters</h3>
    <h4>Keyword</h4>

    <form action="/item/" class="form-inline" id="searchForm" method="post">
        <input type="hidden" name="command" value="ShowItems">
        <div class="control-group">
            <div class="controls">
                <input type="text" class="input-xlarge" id="inputKeyword" name="keyword" placeholder="Keyword">
                <select name="selectBy">
                    <option value=""></option>
                    <option value="byId">by id</option>
                    <option value="byTitle">by title</option>
                    <option value="byDesc">by description</option>
                </select>
            </div>
        </div>
    </form>

    <br/>
    <button class="btn btn-large btn-block btn-primary" type="submit" form="searchForm" value="search" name="search">
        Search
    </button>
</div>

<div>
    <% if (session.getAttribute("uid") != null) { %>
    <% Integer id = (Integer) session.getAttribute("uid");
        if (id == -1) { %>

    <% } else { %>
    <div><a href="<%= request.getRemoteAddr() + "/item/?command=ShowMyItems" %>">Show My Items</a></div>
    </b>
<div><a href="<%= request.getRemoteAddr() + "/user/?command=Logout" %>">Logout</a></div>

<% } %>
<% } else { %>
<div><a href="<%= request.getRemoteAddr() + "/user/?command=Login" %>">Login</a></div>
<% } %>



<div><a href="/">Show All Items</a></div>
<div><a href="/">Sell</a></div>
</div>

<div>
    <table class="table table-striped table-bordered">
        <thead>
        <tr class="info" align="center">
            <td colspan="9">Items</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>

</div>
<jsp:include page="ScriptsIncludes.jsp"/>
</body>
</html>

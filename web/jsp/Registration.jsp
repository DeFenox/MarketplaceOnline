<%--
  Created by IntelliJ IDEA.
  User: DeFenox
  Date: 10.12.2016
  Time: 20:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <jsp:include page="IncludeFiles.jsp"/>
</head>
<body>
<div class="container">
    <form action="/user/" class="form-singin" id="loginForm" method="post">
        <input type="hidden" name="command" value="Registration">
        <div class="control-group">
            <label class="control-label" for="inputFullName">Full name</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="inputFullName" name="fullName"> <br/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputAddress">Billing address</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="inputAddress" name="billingAddress"> <br/>
            </div>
        </div>
        <br/>
        <div class="control-group">
            <label class="control-label" for="inputLogin">Login</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="inputLogin" name="login" placeholder="Login"> <br/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPassword">Password</label>
            <div class="controls">
                <input type="password" class="input-xlarge" id="inputPassword" name="password"> <br/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputRepassword">Re-enter password</label>
            <div class="controls">
                <input type="password" class="input-xlarge" id="inputRepassword" name="re-password"> <br/>
            </div>
        </div>
    </form>
    <br/>
    <button class="btn btn-large btn-block btn-primary" type="submit" form="loginForm" value="registration" name="registration">Registration</button>
    <button class="btn btn-large btn-block" type="submit" form="loginForm" name="reset" value="reset">Reset</button>
</div>


<jsp:include page="ScriptsIncludes.jsp"/>
</body>
</html>

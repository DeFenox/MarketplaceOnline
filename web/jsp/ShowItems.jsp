<%@ page import="com.defenox.marketplace.models.Item" %>
<%@ page import="java.util.List" %>
<%@ page import="com.defenox.marketplace.models.Bid" %><%--
  Created by IntelliJ IDEA.
  User: DeFenox
  Date: 10.12.2016
  Time: 20:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ShowItems</title>
    <jsp:include page="IncludeFiles.jsp"/>
</head>
<body>
<div class="container">
    <div>
        <% if (session.getAttribute("uid") != null) { %>
        <div> You are logged in as
            <% Integer id = (Integer) session.getAttribute("uid");
                if (id == -1) { %>
            Guest
        </div>
        <div><a href="<%= "http://" + request.getServerName() + ":" +  request.getServerPort() + "/user/?command=Login" %>">Login</a></div>

                <% } else { %>
        <b><%= session.getAttribute("uname") %>
        </b>
    </div>
    <div><a href="<%= "http://" + request.getServerName() + ":" +  request.getServerPort() + "/user/?command=Logout" %>">Logout</a></div>

        <% } %>
        <% } else { %>
    <div><a href="<%= "http://" + request.getServerName() + ":" +  request.getServerPort() + "/user/?command=Login" %>">Login</a></div>
        <% } %>
</div>

<div>
    <h1>Online marketplace</h1>
</div>

<div>
    <h3>Search parameters</h3>
    <h4>Keyword</h4>

    <form action="/item/" class="form-search" id="searchForm" method="post">
        <input type="hidden" name="command" value="ShowItems">
        <div class="control-group">
            <div class="controls">
                <input type="text" class="form-control" id="inputKeyword" name="keyword" placeholder="Keyword">
                <select name="selectBy">
                    <option value=""></option>
                    <option value="byTitle">by title</option>
                    <option value="byDesc">by description</option>
                </select>
            </div>
        </div>
    </form>

    <br/>
    <button class="btn btn-large btn-block btn-primary" type="submit" form="searchForm" value="search" name="search">
        Search
    </button>
</div>

<div>
    <% if (session.getAttribute("uid") != null) { %>
        <% Integer id = (Integer) session.getAttribute("uid");
            if (id == -1) { %>

            <% } else { %>
    <div><a href="<%= "http://" + request.getServerName() + ":" +  request.getServerPort() + "/item/?command=ShowMyItems" %>">Show My Items</a></div>
    </b>

<div><a href="<%= "http://" + request.getServerName() + ":" +  request.getServerPort() + "/user/?command=Logout" %>">Logout</a></div>
<% }} %>
</div>

<br/>
<br/>
<br/>
<div>
    <table class="table table-striped table-bordered">
        <thead>
        <tr class="info" align="center">
            <td colspan="9">Items</td>
        </tr>
        <tr>
            <td>Title</td>
            <td>Description</td>
            <td>Seller</td>
            <td>Start price</td>
            <td>Bid inc</td>
            <td>Best offer</td>
            <td>Bidder</td>
            <td>Stop date</td>
            <td>Bidding</td>
        </tr>
        </thead>
        <tbody>

        <%!

            public Bid getMaxBidByItem(List<Bid> bids, Item item){
                Bid resultBid = null;
                int maxBid = 0;
                for (Bid bid: bids){
                    if(bid.getItem().equals(item)) {
                        if (bid.getBid() > maxBid) {
                            resultBid = bid;
                        }
                    }
                }
                return resultBid;
            }
        %>

            <%
                List<Item> items = (List<Item>) request.getAttribute("items");
                List<Bid> bids = (List<Bid>) request.getAttribute("bids");

                if(!items.isEmpty()) {
                    for (Item item : items) {

                        Bid bid = getMaxBidByItem(bids, item);

                        out.println("<tr>");
                        out.println("<td>" + item.getTitle() + "</td>");
                        out.println("<td>" + item.getDescription() + "</td>");
                        out.println("<td>" + item.getSeller().getFullName() + "</td>");
                        out.println("<td>" + item.getStartPrice() + "</td>");
                        out.println("<td>" + item.getBidIncrement() + "</td>");
                        if(bid != null){
                            out.println("<td>" + bid.getBid() + "</td>");
                            out.println("<td>" + bid.getUser().getFullName() + "</td>");
                        }else {
                            out.println("<td></td>");
                            out.println("<td></td>");
                        }
                        out.println("<td>" + item.getStartBiddingDate() + item.getTimeLeft() + "</td>");
                        out.println("<td>" + item.getTitle() + "</td>");
                        out.println("</tr>");
                    }
                }else {
                    out.println("<tr>");
                    out.println("</tr>");
                }
            %>

        </tbody>
    </table>
</div>
<br/>
<br/>
<div>
    <%
        if(request.getAttribute("page") != null) {
            int currentPage = (int) request.getAttribute("page");
            int maxPage = (int) request.getAttribute("maxPage");
            if (maxPage > 1) {
                out.println("<ul class=\"pagination\">");
                if (maxPage < 5 || currentPage < 3) {
                    for (int i = 1; i <= maxPage; i++) {
                        if (i == currentPage) {
                            out.println("<li class=\"active\"><a href=\"/item/?command=ShowItems&page=" + i + "\">" + i + "</a></li>");
                        } else {
                            out.println("<li><a href=\"/item/?command=ShowItems&page=" + i + "\">" + i + "</a></li>");
                        }
                    }
                } else {
                    if ((maxPage - currentPage) < 3) {
                        for (int i = maxPage - 4; i <= maxPage; i++) {
                            if (i == currentPage) {
                                out.println("<li class=\"active\"><a href=\"/item/?command=ShowItems&page=" + i + "\">" + i + "</a></li>");
                            } else {
                                out.println("<li><a href=\"/item/?command=ShowItems&page=" + i + "\">" + i + "</a></li>");
                            }
                        }
                    } else {
                        for (int i = currentPage - 2; i <= currentPage + 2; i++) {
                            if (i == currentPage) {
                                out.println("<li class=\"active\"><a href=\"/item/?command=ShowItems&page=" + i + "\">" + i + "</a></li>");
                            } else {
                                out.println("<li><a href=\"/item/?command=ShowItems&page=" + i + "\">" + i + "</a></li>");
                            }
                        }
                    }
                }
                out.println("</ul>");
            }
        }
    %>
</div>

</div>
<jsp:include page="ScriptsIncludes.jsp"/>
</body>
</html>

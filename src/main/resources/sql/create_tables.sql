--
-- База данных: `marketplace`
--

-- --------------------------------------------------------

--
-- Структура таблицы `BIDS`
--

CREATE TABLE IF NOT EXISTS `BIDS` (
  `BID_ID` int(11) NOT NULL,
  `ITEM_ID` int(11) NOT NULL,
  `BIDDER_ID` int(11) NOT NULL,
  `BID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ITEMS`
--

CREATE TABLE IF NOT EXISTS `ITEMS` (
  `ITEM_ID` int(11) NOT NULL,
  `SELLER_ID` int(11) NOT NULL,
  `TITLE` varchar(45) DEFAULT NULL,
  `DESCRIPTION` text,
  `START_PRICE` int(11) NOT NULL,
  `TIME_LEFT` datetime DEFAULT NULL,
  `START_BIDDING_DATE` datetime DEFAULT NULL,
  `BUY_IT_NOW` tinyint(1) DEFAULT NULL,
  `BID_INCREMENT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `USERS`
--

CREATE TABLE IF NOT EXISTS `USERS` (
  `USER_ID` int(11) NOT NULL,
  `FULL_NAME` varchar(45) DEFAULT NULL,
  `BILLING_ADDRESS` varchar(45) DEFAULT NULL,
  `LOGIN` varchar(15) NOT NULL,
  `PASSWORD` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `BIDS`
--
ALTER TABLE `BIDS`
  ADD PRIMARY KEY (`BID_ID`),
  ADD UNIQUE KEY `BID_ID_UNIQUE` (`BID_ID`),
  ADD KEY `fk_BIDS_ITEMS_idx` (`ITEM_ID`),
  ADD KEY `fk_BIDS_USERS1_idx` (`BIDDER_ID`);

--
-- Индексы таблицы `ITEMS`
--
ALTER TABLE `ITEMS`
  ADD PRIMARY KEY (`ITEM_ID`),
  ADD UNIQUE KEY `ITEM_ID_UNIQUE` (`ITEM_ID`),
  ADD KEY `fk_ITEMS_USERS1_idx` (`SELLER_ID`);

--
-- Индексы таблицы `USERS`
--
ALTER TABLE `USERS`
  ADD PRIMARY KEY (`USER_ID`),
  ADD UNIQUE KEY `USER_ID_UNIQUE` (`USER_ID`),
  ADD UNIQUE KEY `LOGIN_UNIQUE` (`LOGIN`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--
ALTER TABLE `BIDS`
  MODIFY `BID_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ITEMS`
--
ALTER TABLE `ITEMS`
  MODIFY `ITEM_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `USERS`
--
ALTER TABLE `USERS`
  MODIFY `USER_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `BIDS`
--
ALTER TABLE `BIDS`
  ADD CONSTRAINT `fk_BIDS_ITEMS` FOREIGN KEY (`ITEM_ID`) REFERENCES `ITEMS` (`ITEM_ID`) ON UPDATE SET NULL ON DELETE SET NULL,
  ADD CONSTRAINT `fk_BIDS_USERS1` FOREIGN KEY (`BIDDER_ID`) REFERENCES `USERS` (`USER_ID`) ON UPDATE SET NULL ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `ITEMS`
--
ALTER TABLE `ITEMS`
  ADD CONSTRAINT `fk_ITEMS_USERS1` FOREIGN KEY (`SELLER_ID`) REFERENCES `USERS` (`USER_ID`) ON UPDATE SET NULL ON DELETE SET NULL;

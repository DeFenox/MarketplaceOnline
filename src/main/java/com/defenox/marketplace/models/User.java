package com.defenox.marketplace.models;

import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.Identified;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by tokarev on 21.11.2016.
 */
public class User implements Identified<Integer>{
    private int id;
    private String fullName;
    private String billingAddress;
    private String login;
    private String password;

    public User() {
    }

    /**
     * Set params of user from result set
     * @param rs ResultSet object
     * @throws DaoException extends SQLException
     */
    public User(ResultSet rs) throws DaoException {
        try {
            this.setId(rs.getInt("USER_ID"));
            this.setFullName(rs.getString("FULL_NAME"));
            this.setBillingAddress(rs.getString("BILLING_ADDRESS"));
            this.setLogin(rs.getString("LOGIN"));
            this.setPassword(rs.getString("PASSWORD"));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Return id of user
     * @return Integer object
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set id of user
     * @param id Integer object
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Return full name of user
     * @return String object
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Set full name of user
     * @param fullName String object
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * Return billing address of user
     * @return String object
     */
    public String getBillingAddress() {
        return billingAddress;
    }

    /**
     * Set billing address of user
     * @param billingAddress String object
     */
    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * Return login of user
     * @return String object
     */
    public String getLogin() {
        return login;
    }

    /**
     * Set login of user
     * @param login String object
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * Return password of user
     * @return String object
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set password of user
     * @param password String object
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValid(){
        if(!fullName.isEmpty() && !password.isEmpty() && !login.isEmpty() && !billingAddress.isEmpty()){
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (fullName != null ? !fullName.equals(user.fullName) : user.fullName != null) return false;
        if (billingAddress != null ? !billingAddress.equals(user.billingAddress) : user.billingAddress != null)
            return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        return password != null ? password.equals(user.password) : user.password == null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (fullName != null ? fullName.hashCode() : 0);
        result = 31 * result + (billingAddress != null ? billingAddress.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", billingAddress='" + billingAddress + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

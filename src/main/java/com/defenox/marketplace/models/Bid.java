package com.defenox.marketplace.models;

import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.Identified;
import com.defenox.marketplace.dao.mysql.MysqlItemsDao;
import com.defenox.marketplace.dao.mysql.MysqlUsersDao;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by tokarev on 21.11.2016.
 */
public class Bid implements Identified<Integer> {
    private int id;
    private User user;
    private Item item;
    private int bid;

    public Bid() {
    }

    /**
     * Set params of bid from result set
     * @param rs ResultSet object
     * @throws DaoException extends SQLException
     */
    public Bid(ResultSet rs) throws DaoException {
        try {
            this.setId(rs.getInt("BID_ID"));
            this.setUser(new MysqlUsersDao().getByPK(rs.getInt("BIDDER_ID")));
            this.setItem(new MysqlItemsDao().getByPK(rs.getInt("ITEM_ID")));
            this.setBid(rs.getInt("BID"));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Return id of bid
     * @return Integer object
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set id of bid
     * @param id Integer object
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Return user of bid
     * @return User object
     */
    public User getUser() {
        return user;
    }

    /**
     * Set user of bid
     * @param user User object
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Return item of bid
     * @return Item object
     */
    public Item getItem() {
        return item;
    }

    /**
     * Set item of bid
     * @param item Item object
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * Return bid of bid
     * @return Integer object
     */
    public int getBid() {
        return bid;
    }

    /**
     * Set bid of bid
     * @param bid Integer object
     */
    public void setBid(int bid) {
        this.bid = bid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bid bid1 = (Bid) o;

        if (id != bid1.id) return false;
        if (bid != bid1.bid) return false;
        if (user != null ? !user.equals(bid1.user) : bid1.user != null) return false;
        return item != null ? item.equals(bid1.item) : bid1.item == null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (item != null ? item.hashCode() : 0);
        result = 31 * result + bid;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Bid{" +
                "id=" + id +
                ", user=" + user +
                ", item=" + item +
                ", bid=" + bid +
                '}';
    }
}

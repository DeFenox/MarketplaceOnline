package com.defenox.marketplace.models;

import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.Identified;
import com.defenox.marketplace.dao.mysql.MysqlUsersDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by tokarev on 21.11.2016.
 */
public class Item  implements Identified<Integer> {

    private int id;
    private User seller;
    private String title;
    private String description;
    private int startPrice;
    private Date timeLeft;
    private Date startBiddingDate;
    private boolean buyItNow;
    private int bidIncrement;

    public Item() {
    }

    /**
     * Set params of item from result set
     * @param rs ResultSet object
     * @throws DaoException extends SQLException
     */
    public Item(ResultSet rs) throws DaoException {
        try {
            this.setId(rs.getInt("ITEM_ID"));
            this.setSeller(new MysqlUsersDao().getByPK(rs.getInt("SELLER_ID")));
            this.setTitle(rs.getString("TITLE"));
            this.setDescription(rs.getString("DESCRIPTION"));
            this.setStartPrice(rs.getInt("START_PRICE"));
            this.setTimeLeft(rs.getTime("TIME_LEFT"));
            this.setStartBiddingDate(rs.getTime("START_BIDDING_DATE"));
            this.setBuyItNow(rs.getBoolean("BUY_IT_NOW"));
            this.setBidIncrement(rs.getInt("BID_INCREMENT"));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Return id of item
     * @return Integer object
     */
    public Integer getId() {
        return id;
    }

    /**
     * Set id of item
     * @param id Integer object
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Return seller of item
     * @return User object
     */
    public User getSeller() {
        return seller;
    }

    /**
     * Set seller of item
     * @param user User object
     */
    public void setSeller(User user) {
        this.seller = user;
    }

    /**
     * Return title of item
     * @return String object
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set title of item
     * @param title String object
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Return description of item
     * @return String object
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set id description item
     * @param description String object
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return start price of item
     * @return Integer object
     */
    public int getStartPrice() {
        return startPrice;
    }

    /**
     * Set start price of item
     * @param startPrice Integer object
     */
    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    /**
     * Return time left of item
     * @return Date object
     */
    public Date getTimeLeft() {
        return timeLeft;
    }

    /**
     * Set time left of item
     * @param timeLeft Date object
     * @return Date object
     */
    public Date setTimeLeft(Date timeLeft) {
        this.timeLeft = timeLeft;
        return timeLeft;
    }

    /**
     * Return start binding date of item
     * @return Date object
     */
    public Date getStartBiddingDate() {
        return startBiddingDate;
    }

    /**
     * Set start binding date of item
     * @param startBiddingDate Date object
     */
    public void setStartBiddingDate(Date startBiddingDate) {
        this.startBiddingDate = startBiddingDate;
    }

    /**
     * Return is buy it now of item
     * @return Boolean object
     */
    public boolean isBuyItNow() {
        return buyItNow;
    }

    /**
     * Set is buy it now of item
     * @param buyItNow Boolean object
     */
    public void setBuyItNow(boolean buyItNow) {
        this.buyItNow = buyItNow;
    }

    /**
     * Return bid increment of item
     * @return Integer object
     */
    public int getBidIncrement() {
        return bidIncrement;
    }

    /**
     * Set bid increment of item
     * @param bidIncrement Integer object
     */
    public void setBidIncrement(int bidIncrement) {
        this.bidIncrement = bidIncrement;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (id != item.id) return false;
        if (startPrice != item.startPrice) return false;
        if (buyItNow != item.buyItNow) return false;
        if (bidIncrement != item.bidIncrement) return false;
        if (seller != null ? !seller.equals(item.seller) : item.seller != null) return false;
        if (title != null ? !title.equals(item.title) : item.title != null) return false;
        return description != null ? description.equals(item.description) : item.description == null;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (seller != null ? seller.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + startPrice;
        result = 31 * result + (buyItNow ? 1 : 0);
        result = 31 * result + bidIncrement;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", seller=" + seller +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", startPrice=" + startPrice +
                ", timeLeft=" + timeLeft +
                ", startBiddingDate=" + startBiddingDate +
                ", buyItNow=" + buyItNow +
                ", bidIncrement=" + bidIncrement +
                '}';
    }
}

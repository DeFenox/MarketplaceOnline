package com.defenox.marketplace.models;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by DeFenox on 20.11.2016.
 */
public class PropertiesCache {

    private static final String DB_PROPERTIES = "db.properties";

    private Properties dbProperties;

    public PropertiesCache(){
        dbProperties = new Properties();
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(DB_PROPERTIES);
        try {
            dbProperties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return property of db.properties
     * @param key String object
     * @return String object
     */
    public String getProperty(String key) {
        return dbProperties.getProperty(key);
    }
}

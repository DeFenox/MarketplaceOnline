package com.defenox.marketplace.controllers;

import com.defenox.marketplace.controllers.commands.UnknownCommand;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by DeFenox on 11.12.2016.
 */
public abstract class FrontController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FrontCommand command = getParam(request);
        command.init(getServletContext(), request, response);
        command.process();
    }

    private FrontCommand getParam(HttpServletRequest request) {

        try {
            Class type = Class.forName(String.format("com.defenox.marketplace.controllers.commands.%s.%sCommand",
                    getControllerName(), request.getParameter("command")));

            return (FrontCommand) type.asSubclass(FrontCommand.class).newInstance();
        } catch (Exception e) {
            return new UnknownCommand();
        }
    }

    protected abstract String getControllerName();
}

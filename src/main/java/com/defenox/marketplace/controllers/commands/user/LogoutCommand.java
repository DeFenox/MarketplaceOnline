package com.defenox.marketplace.controllers.commands.user;

import com.defenox.marketplace.controllers.FrontCommand;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by tokarev on 19.12.2016.
 */
public class LogoutCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("uname", null);
        session.setAttribute("uid", null);
        session.setAttribute("ulogin", null);
        session.invalidate();
        forward("ShowItems");
    }
}

package com.defenox.marketplace.controllers.commands.user;

import com.defenox.marketplace.controllers.FrontCommand;
import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.UsersDao;
import com.defenox.marketplace.dao.mysql.MysqlUsersDao;
import com.defenox.marketplace.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by tokarev on 15.12.2016.
 */
public class RegistrationCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        if (request.getParameter("registration") != null) {
            User user = new User();
            if (request.getParameter("fullName") != null) {
                if (request.getParameter("fullName").length() >= 3) {
                    user.setFullName(request.getParameter("fullName"));
                }
            }
            if (request.getParameter("billingAddress") != null) {
                if (request.getParameter("billingAddress").length() >= 3) {
                    user.setBillingAddress(request.getParameter("billingAddress"));
                }
            }
            if (request.getParameter("login") != null) {
                if (request.getParameter("login").length() >= 4) {
                    user.setLogin(request.getParameter("login"));
                }
            }
            if (request.getParameter("password") != null) {
                String pass = request.getParameter("password");
                if (pass.length() >= 6) {
                    if (request.getParameter("re-password") != null) {
                        if (pass.equals(request.getParameter("re-password"))) {
                            user.setPassword(pass);
                        }
                    }
                }
            }
            if(user.isValid()) {
                UsersDao usersDao = new MysqlUsersDao();
                try {
                    User us = usersDao.create(user);
                    HttpSession session = request.getSession();
                    session.setAttribute("uname", us.getFullName());
                    session.setAttribute("uid", us.getId());
                    session.setAttribute("ulogin", us.getLogin());
                    response.sendRedirect("/item/?command=ShowItems");
                } catch (DaoException e) {
                    e.printStackTrace();
                }
            }
        }
        forward("Registration");
    }
}
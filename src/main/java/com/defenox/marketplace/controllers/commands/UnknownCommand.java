package com.defenox.marketplace.controllers.commands;

import com.defenox.marketplace.controllers.FrontCommand;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Created by DeFenox on 11.12.2016.
 */
public class UnknownCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        forward("Unknown");
    }
}

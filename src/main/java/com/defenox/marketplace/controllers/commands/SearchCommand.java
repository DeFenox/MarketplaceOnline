package com.defenox.marketplace.controllers.commands;

import com.defenox.marketplace.controllers.FrontCommand;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by DeFenox on 11.12.2016.
 */
public class SearchCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        /*Book book = new BookshelfImpl().getInstance()
                .findByTitle(request.getParameter("title"));
        if (book != null) {
            request.setAttribute("book", book);
            forward("book-found");
        } else {
            forward("book-notfound");
        }*/
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()){
            System.out.println(params.nextElement());
        }
        forward("Login");
    }
}

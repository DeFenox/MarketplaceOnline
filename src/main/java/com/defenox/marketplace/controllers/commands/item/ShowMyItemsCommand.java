package com.defenox.marketplace.controllers.commands.item;

import com.defenox.marketplace.controllers.FrontCommand;
import com.defenox.marketplace.dao.BidsDao;
import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.ItemsDao;
import com.defenox.marketplace.dao.mysql.MysqlBidsDao;
import com.defenox.marketplace.dao.mysql.MysqlItemsDao;
import com.defenox.marketplace.models.Bid;
import com.defenox.marketplace.models.Item;
import com.defenox.marketplace.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.swing.text.html.HTMLDocument;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by tokarev on 15.12.2016.
 */
public class ShowMyItemsCommand extends FrontCommand {

    private List<Bid> bids;
    private List<Item> items;

    private ItemsDao itemsDao;
    private BidsDao bidsDao;

    private int maxNumberOfPages;

    @Override
    public void process() throws ServletException, IOException {

        bids = new ArrayList<>();
        items = new ArrayList<>();

        itemsDao = new MysqlItemsDao();
        bidsDao = new MysqlBidsDao();

        items = getMyItems();

        int pageSize = 5;

        //items = new ArrayList<>();

        if(items.isEmpty()){
            forward("ShowMyItems");
        }else{
            if(request.getParameter("search") != null){
                if(request.getParameter("search").equals("search")){
                    if(request.getParameter("keyword") != null){
                        if(request.getParameter("selectBy").equals("byTitle")){
                            String keyword = request.getParameter("keyword");
                            filterByTitle(keyword);
                            maxNumberOfPages = (int) Math.ceil((float)items.size() / (float) pageSize);
                        }
                        if(request.getParameter("selectBy").equals("byDesc")){
                            String keyword = request.getParameter("keyword");
                            filterByDesc(keyword);
                            maxNumberOfPages = (int) Math.ceil((float)items.size() / (float) pageSize);
                        }
                    }
                }
            }
        }

        /*if(request.getParameter("search") != null){
            if(request.getParameter("search").equals("search")){
                if(request.getParameter("keyword") != null){
                    if(request.getParameter("selectBy").equals("byTitle")){
                        try {
                            String keyword = request.getParameter("keyword");
                            items = itemsDao.getAllByTitle(keyword);
                            maxNumberOfPages = (int) Math.ceil((float)items.size() / (float)pageSize);
                        } catch (DaoException e) {
                            e.printStackTrace();
                        }
                    }
                    if(request.getParameter("selectBy").equals("byDesc")){
                        try {
                            items = itemsDao.getAllByDescription(request.getParameter("keyword"));
                            maxNumberOfPages = (int) Math.ceil((float)items.size() / (float)pageSize);
                        } catch (DaoException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }*/

        /*if(request.getParameter("ShowMyItems") != null){
            User user = null;

            if(items.isEmpty()){
                try {
                    user = usersDao.getByPK((Integer) request.getSession().getAttribute("uid"));
                    items = itemsDao.getAllBySeller(user);
                } catch (DaoException e) {
                    e.printStackTrace();
                }
            }else{
                try {
                    user = usersDao.getByPK((Integer) request.getSession().getAttribute("uid"));
                    Iterator<Item> iter = items.iterator();
                    while (iter.hasNext()){
                        Item item = iter.next();
                        if(item.getSeller().equals(user)){
                            iter.remove();
                        }
                    }
                } catch (DaoException e) {
                    e.printStackTrace();
                }
            }
        }*/

        if(request.getParameter("page") != null){
            Integer pageCurrent = Integer.parseInt(request.getParameter("page"));

            if(items.isEmpty()){

                if(pageCurrent > 0){
                    request.setAttribute("page", pageCurrent);
                    itemsDao.getCount();
                    if(pageCurrent == 1){
                        pageCurrent = 0;
                    }else {
                        pageCurrent = ((pageCurrent - 1) * pageSize);
                    }
                    try {
                        items = itemsDao.getRecords(pageCurrent, pageSize);
                    } catch (DaoException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        items = itemsDao.getRecords(0, pageSize);
                        request.setAttribute("page", 1);
                    } catch (DaoException e) {
                        e.printStackTrace();
                    }
                }
            }else{

                if(pageCurrent == 0){
                    request.setAttribute("page", 1);
                    while (items.size() > pageSize - 1){
                        items.remove(items.size() - 1);
                    }
                }else{
                    while (items.size() > (pageCurrent * pageSize - 1)){
                        items.remove(items.size() - 1);
                    }
                    int count = ((pageCurrent - 1) * pageSize) - 1;
                    for(int i = 0; i < count; i++ ){
                        items.remove(0);
                    }
                }
            }
        }

        /*if(request.getParameter("search") == null && items.isEmpty()){
            try {
                maxNumberOfPages = (int) Math.ceil(itemsDao.getCount() / pageSize);
                items = itemsDao.getRecords(0, pageSize);
            } catch (DaoException e) {
                e.printStackTrace();
            }
            request.setAttribute("page", 1);
        }*/

        if(bids.isEmpty()){
            Iterator<Item> iter = items.iterator();
            while (iter.hasNext()){
                try {
                    bids.addAll(bidsDao.getAllByItem(iter.next()));
                } catch (DaoException e) {
                    e.printStackTrace();
                }
            }
        }
        request.setAttribute("maxPage", maxNumberOfPages);
        request.setAttribute("items", items);
        request.setAttribute("bids", bids);

        forward("ShowMyItems");
    }

    private void filterByDesc(String keyword) {
        Iterator<Item> iter = items.iterator();
        HttpSession session = request.getSession();
        while (iter.hasNext()) {
            Item it = iter.next();
            if (!it.getDescription().contains(keyword)){
                iter.remove();
            }
        }
    }

    private void filterByTitle(String keyword){
        Iterator<Item> iter = items.iterator();
        HttpSession session = request.getSession();
        while (iter.hasNext()) {
            Item it = iter.next();
            if (!it.getTitle().contains(keyword)){
                iter.remove();
            }
        }
    }

    private List<Item> getMyItems() {
        HttpSession session = request.getSession();
        if(session.getAttribute("uid") != null) {
            try {
                User user = new User();
                user.setId((Integer) session.getAttribute("uid"));
                return itemsDao.getAllBySeller(user);
            } catch (DaoException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }
}

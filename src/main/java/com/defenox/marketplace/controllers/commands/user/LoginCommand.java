package com.defenox.marketplace.controllers.commands.user;

import com.defenox.marketplace.controllers.FrontCommand;
import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.UsersDao;
import com.defenox.marketplace.dao.mysql.MysqlUsersDao;
import com.defenox.marketplace.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by tokarev on 15.12.2016.
 */
public class LoginCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        if (request.getParameter("guest") != null) {
            //TODO refactor this, create this into AuthFilter
            HttpSession session = request.getSession();
            session.setAttribute("userId", -1);
            response.sendRedirect("/item/?command=ShowItems");
        } else if (request.getParameter("register") != null) {
            forward("Registration");
        } else if (request.getParameter("singin") != null) {

            if (request.getParameter("login") != "" && request.getParameter("password") != "") {
                UsersDao usersDao = new MysqlUsersDao();
                try {
                    User user = usersDao.getAllByLogin(request.getParameter("login"));
                    if(user != null) {
                        if (user.getPassword().equals(request.getParameter("password"))) {
                            HttpSession session = request.getSession();
                            session.setAttribute("uname", user.getFullName());
                            session.setAttribute("uid", user.getId());
                            session.setAttribute("ulogin", user.getLogin());
                            response.sendRedirect("/item/?command=ShowItems");
                        } else {
                            //TODO to do create about error
                            System.out.println("wrong password");
                        }
                    }else {
                        //TODO to do exception about wrong login
                    }
                } catch (DaoException e) {
                    e.printStackTrace();
                }
            } else {
                //TODO to do later
                System.out.println("not found login and password");
                //TODO to do later
                forward("Login");
            }
        }else {
            forward("Login");
        }
    }
}

package com.defenox.marketplace.controllers.commands.item;

import com.defenox.marketplace.controllers.FrontCommand;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Created by tokarev on 15.12.2016.
 */
public class EditItemCommand extends FrontCommand {
    @Override
    public void process() throws ServletException, IOException {
        if(request.getParameter("id") != null){

            forward("EditItem");
        }
        forward("Unknown");

    }
}

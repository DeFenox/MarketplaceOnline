package com.defenox.marketplace.controllers;

import javax.servlet.annotation.WebServlet;

/**
 * Created by tokarev on 20.12.2016.
 */
@WebServlet("/UserController")
public class UserController extends FrontController {
    @Override
    protected String getControllerName() {
        return "user";
    }
}

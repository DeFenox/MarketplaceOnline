package com.defenox.marketplace.dao;

import com.defenox.marketplace.dao.mysql.MysqlDaoFactory;

import java.sql.Connection;

/**
 * Created by DeFenox on 20.11.2016.
 */
public abstract class DaoFactory {

    public static final int MYSQL = 1;

    /**
     * Return UsersDao child
     * @return UsersDao object
     */
    public abstract UsersDao getUsersDAO();

    /**
     * Return BidsDao child
     * @return BidsDao object
     */
    public abstract BidsDao getBidsDAO();

    /**
     * Return ItemsDao child
     * @return ItemsDao object
     */
    public abstract ItemsDao getItemsDAO();

    /**
     * Return DAOFactory
     * @param whichFactory Integer
     * @return DAOFactory child
     */
    public static DaoFactory getDAOFactory(int whichFactory){
        switch(whichFactory){
            case MYSQL :
                return new MysqlDaoFactory();
            default:
                return null;
        }
    }

    /**
     * Return connection with databases
     * @return Connection object
     */
    public abstract Connection createConnection();
}

package com.defenox.marketplace.dao;

/**
 * Created by tokarev on 01.12.2016.
 */

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 * @param <T> type object
 * @param <PK> type primary key
 */
public interface GenericDao<T extends Identified<PK>, PK extends Serializable> {

    /**
     * Create object into database
     */
    public T create(T object)  throws SQLException;

    /**
     * Get object by primary key
     */
    public T getByPK(Integer key) throws SQLException;

    /**
     * Update object into database
     */
    public void update(T object) throws SQLException;

    /**
     * Delete object into database
     */
    public void delete(T object) throws SQLException;

    /**
     * Get list of objects from database
     */
    public List<T> getAll() throws SQLException;
}

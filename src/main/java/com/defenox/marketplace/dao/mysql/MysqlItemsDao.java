package com.defenox.marketplace.dao.mysql;

import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.ItemsDao;
import com.defenox.marketplace.models.Item;
import com.defenox.marketplace.models.User;

import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeFenox on 20.11.2016.
 */
public class MysqlItemsDao extends MysqlDao<Item, Integer> implements ItemsDao {

    private static final String SELECT_ALL_ITEMS = "SELECT * FROM ITEMS";
    private static final String INSERT_ITEM = "INSERT INTO ITEMS (SELLER_ID, TITLE, DESCRIPTION, START_PRICE, TIME_LEFT, START_BIDDING_DATE, BUY_IT_NOW, BID_INCREMENT) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_ITEM = "UPDATE ITEMS SET SELLER_ID = ?, TITLE = ?, DESCRIPTION = ?, START_PRICE = ?, TIME_LEFT = ?, START_BIDDING_DATE = ?, BUY_IT_NOW = ?, BID_INCREMENT = ? WHERE ITEM_ID = ?";
    private static final String DELETE_ITEM = "DELETE FROM ITEMS WHERE ITEM_ID = ?";
    private static final String SELECT_ITEM_BY_ID = "SELECT * FROM ITEMS WHERE ITEM_ID = ?";
    private static final String SELECT_ALL_ITEMS_BY_TITLE = "SELECT * FROM ITEMS WHERE TITLE LIKE ?";
    private static final String SELECT_ALL_ITEMS_BY_DESCRIPTION = "SELECT * FROM ITEMS WHERE DESCRIPTION LIKE ?";
    private static final String SELECT_ALL_ITEMS_BY_SELLER = "SELECT * FROM ITEMS WHERE SELLER_ID = ?";

    /**
     * {@inheritDoc}
     */
    @Override
    protected Connection getConnection() {
        return super.getConnection();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSelectAllQuery() {
        return SELECT_ALL_ITEMS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreateQuery() {
        return INSERT_ITEM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdateQuery() {
        return UPDATE_ITEM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDeleteQuery() {
        return DELETE_ITEM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSelectQueryByPK() {
        return SELECT_ITEM_BY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Item> parseResultSet(ResultSet rs) throws SQLException {
        List<Item> items = new ArrayList<>();
        while (rs.next()) {
            items.add(getEntityFromResultSet(rs));
        }
        return items;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Item getEntityFromResultSet(ResultSet rs) throws SQLException {
        return new Item(rs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Item item) throws SQLException, ParseException {
        statement.setInt(1, item.getSeller().getId());
        statement.setString(2, item.getTitle());
        statement.setString(3, item.getDescription());
        statement.setInt(4, item.getStartPrice());
        statement.setTimestamp(5, new java.sql.Timestamp(item.getTimeLeft().getTime()));
        statement.setTimestamp(6, new java.sql.Timestamp(item.getStartBiddingDate().getTime()));
        statement.setBoolean(7, item.isBuyItNow());
        statement.setInt(8, item.getBidIncrement());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Item item) throws SQLException {
        statement.setInt(1, item.getSeller().getId());
        statement.setString(2, item.getTitle());
        statement.setString(3, item.getDescription());
        statement.setInt(4, item.getStartPrice());
        statement.setTimestamp(5, new java.sql.Timestamp(item.getTimeLeft().getTime()));
        statement.setTimestamp(6, new java.sql.Timestamp(item.getStartBiddingDate().getTime()));
        statement.setBoolean(7, item.isBuyItNow());
        statement.setInt(8, item.getBidIncrement());
        statement.setInt(9, item.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Item getByPK(Integer key) throws DaoException {
        return super.getByPK(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> getAll() throws DaoException {
        return super.getAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> getAllByTitle(String title) throws DaoException {
        List<Item> list;
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_ITEMS_BY_TITLE)) {
            statement.setString(1, "%" + title + "%");
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> getAllByDescription(String description) throws DaoException {
        List<Item> list;
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_ITEMS_BY_DESCRIPTION)) {
            statement.setString(1, "%" + description + "%");
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Item> getAllBySeller(User user) throws DaoException {
        List<Item> list;
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_ITEMS_BY_SELLER)) {
            statement.setInt(1, user.getId());
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Item create(Item item) throws DaoException {
        return super.create(item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Item item) throws DaoException {
        super.update(item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Item item) throws DaoException {
        super.delete(item);
    }

    @Override
    public List<Item> getRecords(int page, int pageSize) throws DaoException {
        List<Item> list;
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM ITEMS LIMIT " + page + ",  " + pageSize)) {
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return list;
    }

    @Override
    public int getCount() {
        Connection connection = getConnection();

        ResultSet resultSet;
        try(Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery("SELECT count(*) from ITEMS");
            if(resultSet.next()) {
                return resultSet.getInt(1);
            }else {
                return 0;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
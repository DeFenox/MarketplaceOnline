package com.defenox.marketplace.dao.mysql;

import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.UsersDao;
import com.defenox.marketplace.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeFenox on 20.11.2016.
 */
public class MysqlUsersDao extends MysqlDao<User, Integer> implements UsersDao {

    private static final String SELECT_FROM_USERS = "SELECT * FROM USERS";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM USERS WHERE USER_ID = ?";
    private static final String SELECT_USERS_BY_LOGIN = "SELECT * FROM USERS WHERE LOGIN LIKE ?";
    private static final String INSERT_USER = "INSERT INTO USERS (FULL_NAME, BILLING_ADDRESS, LOGIN, PASSWORD) VALUES (?, ?, ?, ?)";
    private static final String UPDATE_USER = "UPDATE USERS SET FULL_NAME = ?, BILLING_ADDRESS = ?, LOGIN = ?, PASSWORD = ? WHERE USER_ID = ?";
    private static final String DELETE_USER = "DELETE FROM USERS WHERE USER_ID = ?";

    /**
     * {@inheritDoc}
     */
    @Override
    protected Connection getConnection() {
        return super.getConnection();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSelectAllQuery() {
        return SELECT_FROM_USERS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSelectQueryByPK() {
        return SELECT_USER_BY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreateQuery() {
        return INSERT_USER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdateQuery() {
        return UPDATE_USER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDeleteQuery() {
        return DELETE_USER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<User> parseResultSet(ResultSet rs) throws SQLException {
        List<User> users = new ArrayList<>();
        while (rs.next()) {
            users.add(getEntityFromResultSet(rs));
        }
        return users;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected User getEntityFromResultSet(ResultSet rs) throws SQLException {
        return new User(rs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, User user) throws SQLException {
        statement.setString(1, user.getFullName());
        statement.setString(2, user.getBillingAddress());
        statement.setString(3, user.getLogin());
        statement.setString(4, user.getPassword());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, User user) throws SQLException {
        statement.setString(1, user.getFullName());
        statement.setString(2, user.getBillingAddress());
        statement.setString(3, user.getLogin());
        statement.setString(4, user.getPassword());
        statement.setInt(5, user.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getByPK(Integer key) throws DaoException {
        return super.getByPK(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> getAll() throws DaoException {
        return super.getAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getAllByLogin(String login) throws DaoException {
        Connection connection = getConnection();
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(SELECT_USERS_BY_LOGIN)) {
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                user = getEntityFromResultSet(rs);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User create(User object) throws DaoException {
        return super.create(object);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(User object) throws DaoException {
        super.update(object);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(User object) throws DaoException {
        super.delete(object);
    }
}

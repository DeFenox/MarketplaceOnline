package com.defenox.marketplace.dao.mysql;

import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.DaoFactory;
import com.defenox.marketplace.dao.GenericDao;
import com.defenox.marketplace.dao.Identified;

import java.sql.*;
import java.text.ParseException;
import java.util.List;

/**
 * Created by tokarev on 23.11.2016.
 */

public abstract class MysqlDao<T extends Identified<PK>, PK extends Integer> implements GenericDao<T, PK> {

    /**
     * {@inheritDoc}
     */
    protected Connection getConnection() {
        return DaoFactory.getDAOFactory(DaoFactory.MYSQL).createConnection();
    }

    /**
     * {@inheritDoc}
     */
    public abstract String getSelectAllQuery();

    /**
     * {@inheritDoc}
     */
    protected abstract String getSelectQueryByPK();

    /**
     * {@inheritDoc}
     */
    public abstract String getCreateQuery();

    /**
     * {@inheritDoc}
     */
    public abstract String getUpdateQuery();

    /**
     * {@inheritDoc}
     */
    public abstract String getDeleteQuery();

    /**
     * {@inheritDoc}
     */
    protected abstract List<T> parseResultSet(ResultSet rs) throws SQLException;

    /**
     * {@inheritDoc}
     */
    protected abstract T getEntityFromResultSet(ResultSet rs) throws SQLException;

    /**
     * {@inheritDoc}
     */
    protected abstract void prepareStatementForInsert(PreparedStatement statement, T object) throws SQLException, ParseException;

    /**
     * {@inheritDoc}
     */
    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object) throws SQLException;

    /**
     * {@inheritDoc}
     */
    @Override
    public T getByPK(Integer key) throws DaoException {
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(getSelectQueryByPK())) {
            statement.setInt(1, key);
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                return getEntityFromResultSet(rs);
            }else {
                return null;
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getAll() throws DaoException {
        List<T> list;
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(getSelectAllQuery())) {
            ResultSet rs = statement.executeQuery();
            list = parseResultSet(rs);
        } catch (Exception e) {
            throw new DaoException(e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T create(T object) throws DaoException {
        if (!object.getId().equals(0)) {
            throw new DaoException("Object is already exist.");
        }
        String sql = getCreateQuery();
        int elementId = 0;
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatementForInsert(statement, object);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("On create modify more then 1 record: " + count);
            }
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()){
                elementId = rs.getInt(1);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return getByPK(elementId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(T object) throws DaoException {
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(getUpdateQuery())) {
            prepareStatementForUpdate(statement, object);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DaoException("On update modify more then 1 record: " + count);
            }
        } catch (Exception e) {
            throw new DaoException(e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(T object) throws DaoException {
        Connection connection = getConnection();
        try (PreparedStatement statement = connection.prepareStatement(getDeleteQuery())) {
            try {
                statement.setObject(1, object.getId());
            } catch (Exception e) {
                throw new DaoException(e);
            }
            int count = statement.executeUpdate();
            if (count < 1) {
                throw new DaoException("On delete modify more then 1 record: " + count);
            }
            statement.close();
        } catch (Exception e) {
            throw new DaoException(e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
    }
}

package com.defenox.marketplace.dao.mysql;

import com.defenox.marketplace.dao.BidsDao;
import com.defenox.marketplace.dao.DaoException;
import com.defenox.marketplace.dao.Identified;
import com.defenox.marketplace.models.Bid;
import com.defenox.marketplace.models.Item;
import com.defenox.marketplace.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DeFenox on 20.11.2016.
 */
public class MysqlBidsDao extends MysqlDao<Bid, Integer> implements BidsDao {

    private static final String SELECT_ALL_BIDS = "SELECT * FROM BIDS";
    private static final String SELECT_BID_BY_PK = "SELECT * FROM BIDS WHERE BID_ID = ?";
    private static final String SELECT_ALL_BIDS_BY_BIDDER = "SELECT * FROM BIDS WHERE BIDDER_ID = ?";
    private static final String SELECT_ALL_BIDS_BY_ITEM = "SELECT * FROM BIDS WHERE ITEM_ID = ?";
    private static final String INSERT_BID = "INSERT INTO BIDS (BIDDER_ID, ITEM_ID, BID) VALUES (?, ?, ?)";
    private static final String UPDATE_BID = "UPDATE ITEMS SET BIDDER_ID = ?, ITEM_ID = ?, BID = ? WHERE BID_ID = ?";
    private static final String DELETE_BID = "DELETE FROM BIDS WHERE BID_ID = ?";

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSelectAllQuery() {
        return SELECT_ALL_BIDS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSelectQueryByPK() {
        return SELECT_BID_BY_PK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreateQuery() {
        return INSERT_BID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUpdateQuery() {
        return UPDATE_BID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDeleteQuery() {
        return DELETE_BID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Bid> parseResultSet(ResultSet rs) throws SQLException {
        List<Bid> bids = new ArrayList<>();
        while (rs.next()) {
            bids.add(getEntityFromResultSet(rs));
        }
        return bids;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Bid getEntityFromResultSet(ResultSet rs) throws SQLException {
        return new Bid(rs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Bid bid) throws SQLException {
        statement.setInt(1, bid.getUser().getId());
        statement.setInt(2, bid.getItem().getId());
        statement.setInt(3, bid.getBid());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Bid bid) throws SQLException {
        statement.setInt(1, bid.getUser().getId());
        statement.setInt(2, bid.getItem().getId());
        statement.setInt(3, bid.getBid());
        statement.setInt(4, bid.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Bid create(Bid bid) throws DaoException {
        return super.create(bid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Bid bid) throws DaoException {
        super.update(bid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Bid bid) throws DaoException {
        super.delete(bid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Bid> getAll() throws DaoException {
        return super.getAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Bid> getAllByBidder(User user) throws DaoException {
        return getAllBy(user, SELECT_ALL_BIDS_BY_BIDDER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Bid> getAllByItem(Item item) throws DaoException {
        return getAllBy(item, SELECT_ALL_BIDS_BY_ITEM);
    }

    private List<Bid> getAllBy(Identified object, String sql) throws DaoException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement;
        List<Bid> entities = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, (Integer) object.getId());
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                entities.add(getEntityFromResultSet(rs));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
        }
        return entities;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Bid makeBid(Bid bid) throws DaoException {
        List<Bid> bidsByItem = getAllByItem(bid.getItem());
        Bid maxBid = getMaxBidOfItem(bidsByItem);

        bid.setBid(maxBid.getBid() + bid.getItem().getBidIncrement());

        if(getByPK(bid.getId()) != null){
            update(bid);
        }else {
            create(bid);
        }
        return bid;
    }

    private Bid getMaxBidOfItem(List<Bid> bidsByItem) {
        int max = 0;
        Bid maxBid = null;
        for (Bid bid : bidsByItem) {
            if(bid.getBid() > max){
                max = bid.getBid();
                maxBid = bid;
            }
        }
        return maxBid;
    }
}

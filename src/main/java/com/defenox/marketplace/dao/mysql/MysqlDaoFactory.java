package com.defenox.marketplace.dao.mysql;

import com.defenox.marketplace.models.PropertiesCache;
import com.defenox.marketplace.dao.BidsDao;
import com.defenox.marketplace.dao.DaoFactory;
import com.defenox.marketplace.dao.ItemsDao;
import com.defenox.marketplace.dao.UsersDao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by DeFenox on 20.11.2016.
 */
public class MysqlDaoFactory extends DaoFactory {

    public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";

    /**
     * {@inheritDoc}
     */
    public Connection createConnection() {
        PropertiesCache propertiesCache = new PropertiesCache();
        String url = propertiesCache.getProperty("Url");
        String username = propertiesCache.getProperty("Username");
        String password = propertiesCache.getProperty("Password");

        try {
           Class.forName(MYSQL_DRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("MqSQL driver not found");
            e.printStackTrace();
            return null;
        }

        try {
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public UsersDao getUsersDAO() {
        return new MysqlUsersDao();
    }

    /**
     * {@inheritDoc}
     */
    public BidsDao getBidsDAO() {
        return new MysqlBidsDao();
    }

    /**
     * {@inheritDoc}
     */
    public ItemsDao getItemsDAO() {
        return new MysqlItemsDao();
    }
}

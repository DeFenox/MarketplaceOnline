package com.defenox.marketplace.dao;

import com.defenox.marketplace.models.User;

import java.util.List;

/**
 * Created by DeFenox on 20.11.2016.
 */
public interface UsersDao {

    /**
     * {@inheritDoc}
     */
    public User create(User user)  throws DaoException;

    /**
     * {@inheritDoc}
     */
    public User getByPK(Integer key) throws DaoException;

    /**
     * {@inheritDoc}
     */
    public void update(User user) throws DaoException;

    /**
     * {@inheritDoc}
     */
    public void delete(User user) throws DaoException;

    /**
     * Return list of users
     * @return List
     * @throws DaoException extend SQLException
     */
    public List<User> getAll() throws DaoException;

    /**
     * Return list of users by login
     * @param login String object
     * @return List
     * @throws DaoException extend SQLException
     */
    public User getAllByLogin(String login) throws DaoException;
}

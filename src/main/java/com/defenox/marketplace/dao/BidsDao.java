package com.defenox.marketplace.dao;

import com.defenox.marketplace.models.Bid;
import com.defenox.marketplace.models.Item;
import com.defenox.marketplace.models.User;

import java.util.List;

/**
 * Created by DeFenox on 20.11.2016.
 */
public interface BidsDao {
    /**
     * {@inheritDoc}
     */
    Bid create(Bid bid) throws DaoException;

    /**
     * {@inheritDoc}
     */
    void update(Bid bid) throws DaoException;

    /**
     * {@inheritDoc}
     */
    void delete(Bid bid) throws DaoException;

    /**
     * {@inheritDoc}
     */
    List<Bid> getAll() throws DaoException;

    /**
     * Make bit in Bids object and database
     * @param bid Bid object
     * @return Bid object
     * @throws DaoException extend SQLException
     */
    Bid makeBid(Bid bid) throws DaoException;

    /**
     * Returns get list of bids by User
     * @param user User object
     * @return List of bid
     * @throws DaoException extend SQLException
     */
    List<Bid> getAllByBidder(User user) throws DaoException;

    /**
     * Returns get list of bids by Item
     * @param item Item object
     * @return List of bid
     * @throws DaoException extend SQLException
     */
    List<Bid> getAllByItem(Item item) throws DaoException;
}

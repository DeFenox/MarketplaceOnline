package com.defenox.marketplace.dao;

import com.defenox.marketplace.models.Item;
import com.defenox.marketplace.models.User;

import java.util.List;

/**
 * Created by DeFenox on 20.11.2016.
 */
public interface ItemsDao {

    /**
     * {@inheritDoc}
     */
    public Item create(Item item)  throws DaoException;

    /**
     * {@inheritDoc}
     */
    public Item getByPK(Integer key) throws DaoException;

    /**
     * {@inheritDoc}
     */
    public void update(Item item) throws DaoException;

    /**
     * {@inheritDoc}
     */
    public void delete(Item item) throws DaoException;

    /**
     * {@inheritDoc}
     */
    public List<Item> getAll() throws DaoException;

    /**
     * Returns get list of items by title
     * @param title String object
     * @return List of list
     * @throws DaoException extend SQLException
     */
    public List<Item> getAllByTitle(String title) throws DaoException;

    /**
     * Returns get list of items by description
     * @param description String object
     * @return List of list
     * @throws DaoException extend SQLException
     */
    public List<Item> getAllByDescription(String description) throws DaoException;

    /**
     * Returns get list of items by User
     * @param user User object
     * @return List of list
     * @throws DaoException extend SQLException
     */
    public List<Item> getAllBySeller(User user) throws DaoException;

    public List<Item> getRecords(int page, int pageSize) throws DaoException;

    public int getCount();
}

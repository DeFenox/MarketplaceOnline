package com.defenox.marketplace.dao;

import java.io.Serializable;

public interface Identified<PK extends Serializable> {

    /**
     * @return primary key of object
     */
    PK getId();
}

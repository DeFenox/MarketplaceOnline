package com.defenox.marketplace.dao;

import java.sql.SQLException;

/**
 * Created by tokarev on 01.12.2016.
 */
public class DaoException extends SQLException {

    public DaoException() { }

    /**
     * Set message of exception
     * @param msg String object
     */
    public DaoException(String msg) { super(msg); }

    /**
     * Set exception
     * @param e Exception object
     */
    public DaoException(Exception e) { super(e); }
}

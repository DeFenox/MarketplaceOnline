package com.defenox.marketplace.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * Created by tokarev on 01.12.2016.
 */

public abstract class AbstractJDBCDao<T extends Identified<PK>, PK extends Integer> implements GenericDao<T, PK> {

    /**
     * Get query select all object
     * @return String object
     */
    public abstract String getSelectAllQuery();

    /**
     * Get query select object by primary key
     * @return String object
     */
    public abstract String getSelectQueryByPK();

    /**
     * Get query create object
     * @return String object
     */
    public abstract String getCreateQuery();

    /**
     * Get query update object
     * @return String object
     */
    public abstract String getUpdateQuery();

    /**
     * Get query delete object
     * @return String object
     */
    public abstract String getDeleteQuery();

    /**
     * Parse list objects from result set
     * @param rs ResultSet object
     * @return List object
     * @throws DaoException extend SQLException
     */
    protected abstract List<T> parseResultSet(ResultSet rs) throws DaoException;

    /**
     * Get object from result set
     * @param rs ResultSet object
     * @return object
     * @throws DaoException extend SQLException
     */
    protected abstract T getEntityFromResultSet(ResultSet rs) throws DaoException;

    /**
     * Set prepare statement for insert
     * @param statement PreparedStatement object
     * @param object
     * @throws DaoException extend SQLException
     */
    protected abstract void prepareStatementForInsert(PreparedStatement statement, T object) throws DaoException;

    /**
     * Set prepare statement for update
     * @param statement PreparedStatement object
     * @param object
     * @throws DaoException extend SQLException
     */
    protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object) throws DaoException;

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract T getByPK(Integer key) throws DaoException;

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public abstract List<T> getAll() throws DaoException;

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract T create(T object) throws DaoException;

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void update(T object) throws DaoException;

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void delete(T object) throws DaoException;
}
